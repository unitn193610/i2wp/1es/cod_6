package cod_6;
/*
Prima di procedere ricordiamoche:
• un processo è un programma in esecuzione;
• un Thread è un processo che appartiene ad un programma o ad un altro processo.
 */

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.StringTokenizer;
import java.util.stream.Stream;

/*
Anche qui una parte di codice è identica a quanto visto in cod_5.
Cambia solo il metodo run() in qui andremo a scrivere ciò che dovrà richiedere il client.
Infatti in quest'esercizio:

  • TinyHttpd ascolta su una PORT specificata e serve semplici richiese HTTP in sitle "get file":

                                    GET /path/filename [optional stuff]

  • Il Web Browser manda una o più di queste linee per ogni documento da ottenere.

  • Letta la richiesta, il server prova ad aprire il file specificato e ne spedisce il contenuto.

  • Per ragioni di performance, TinyHttd serve per ogni richiesta UN SUO thread.
    Perciò TinyHttpd può servire molte richieste concorrentamente.
 */

public class TinyHttpdConnection extends Thread {

    private final Socket SOCKET;

    public TinyHttpdConnection(Socket s)
    {
        this.SOCKET = s;

        setPriority(NORM_PRIORITY - 1);
        /*
        Un thread è contradistinto da una priorità, ovvero un valore intero
        che ne indichi l'importanza rispetto ad altri. La classe Thread
        definisce il metodo getPriority() per ricavare la priorità di un thread ed
        il corrispettivo setPriority() per impostarla. Se non viene definita alcuna
        priorità, verrà assegnato per default il valore Thread.NORM_PRIORITY.

        In questo caso, abbassando la priorità assicuriamo che il thread che serve
        le connessioni già stabilite non blocchi la main thread di TinyHttpd
        impedendole di accettare nuove richieste.
         */

        initInstance();
    }

    private void initInstance(){
        start(); //facciamo partire il Thread
    }

    @Override
    public void run()
    {
        try{
            //--------------------------------------------------------------------------------
            BufferedReader in = new BufferedReader(new InputStreamReader(SOCKET.getInputStream()));
            //Apriamo il flusso in entrata del socket.

            OutputStream out = SOCKET.getOutputStream();
            //Qui, a differenza degli OutputStream visti precedentemente, dovremmo
            //scrivere delle risposte in termini di "status" (es. 200 OK, 404 Not Found).

            String req = in.readLine(); //req è la stringa in cui andremmo a porre l'intera stringa digitata nell'url
            if(req == null)
                req=""; //Così preveniamo gli errori di NullPointerException
            System.out.println("Request: " + req);

            StringTokenizer st = new StringTokenizer(req);
            /*
                StringTokenizer è una classe che ci permette di suddividere una stringa
                in più token, ovvero un blocco di testo categorizzato
                (in altre parole una sottostringa con un significato logico).
                La manierà più semplice per suddividere una stringa tramite questa classe
                consiste nel passare come argomenti del metodo la string e il carattere delimitatore, es:

                StringTokenizer st = new StringTokenizer("Ciao come va", " ");
                while(st.hasMoreTokens())
			    System.out.println(st.nextToken());

                OUTPUT:
                Ciao
                come
                va
		
                Come possiam vedere StringTokenizer definisce dei metodi, in totale 6
                Tra questi abbiamo il metodo:
                • countTokens(): che appunto analizza la stringa e restituisce la quantità
                        di tokens ancora da analizzare.
                • hasMoreElements(): true se c’è un prossimo token
                • hasMoreTokens(): true se c’è un prossimo token
                • nextElement(): restituisce il token successivo come object
                • nextToken(): restituisce il token successivo come Stringa
             */

            //Estraiamo ora il verbo GET ed almeno un nome o un path
            if((st.countTokens() >= 2) && st.nextToken().equals("GET"))
            {	/*
                il primo token è riferito all’indirizzo dell’host (es. 192.168.1.1)
                il secondo si riferisce alla porta (es. 3000)
                da notare che il carattere ‘:’ viene considerato di default come “separatore”
                */

                //req = 192.168.1.1:3000 GET /index.html

                //Estraiamo il path file trasformandolo in una "variabile"
                if((req = st.nextToken()).startsWith("/"))
                    req = req.substring(1); //req=index.html

                //Se è una directory, di default apriamo il file null.html
                if(req.endsWith("/") || req.isEmpty())
                    req = req + "null.html"; //if req=localhost:3000 GET /, allora req = /null.html

                try
                {
                    Stream<String> stream = Files.lines(Paths.get(req));
                    /*
                    Stream è un'interfaccia che restituisce un flusso di dati finito o infinito
                    su cui è possibile fare operazioni di filtro, mappa e riduzione.
                    Può operare in modo sequenziale (che è il default) oppure a richiesta
                    in modo parallelo. Possiamo vedere lo stream come una pipeline di operazioni
                    su dati che, come ogni tubatura che si rispetti, ha una sorgente, una serie
                    di pezzi intermedi e una destinazione.

                    Nel nostro caso vogliamo una variabile di tipo String in cui andremmo a salvare
                    tutto ciò che è contenuto nel nostro file richiesto (dunque index.html ad esempio).
                    Procedendo per ordine:
                    • Paths.get(req) è una variabile di tipo percorso che tramite il metodo get()
                        converte una stringa, che specifica un percorso, in una variabile
                        di tipo percorso.
                    • Files è una classe per la gestione di file, directory e altri tipi di files.
                      Il metodo lines() permette di leggere tutto ciò che è contenuto all'interno di un file
                      in formato UTF (quindi legge ogni singolo carattere UTF-8).
                      Per far ciò ha bisoggno di avere come argomento un percorso che specifica la posizione
                      del file richiesto. Questo è il motivo per cui ci siamo serviti della classe Path.


                    Per concludere, stiamo salvando tutto il file richiesto, ad esempio
                    stream = localhost:3000/files/index.html
                    in una sola variabile di tipo Stringa.
                    */


                    /*
                    Settiamo lo status a 200 se la richiesta è andata a buon fine.
                    La prima riga di codice che segue viene infatti interpretata dal browser (o simili, vedi Postman)
                    come un messaggio in grado di settare lo status-code e uno stato (stringa).
                    Ricordiamo però che noi stiamo scrivendo nel flusso di uscita della socket.
                    Non tutti i protocolli però sono in grado di intepretare delle variabili di un uno specifico linguaggio
                    pertanto, per evitare errori, è buona norma "convertire" le stringhe in bytes.
                    Ciò viene reso possibile proprio dal metodo getBytes().
                    Quello che stiamo facendo consiste dunque nel memorizzare i caratteri in bytes in un array (di bytes).
                    */
                    out.write("HTTP/1.1 200 OK\r\n".getBytes());

                    /*
                    Tramite questa sintassi riferiamo al browser come interpretare il contenuto che invieremo e,
                    in particolare, la tipologia di testo, in questo caso html.
                    Provando a togliere la parte 'html' potrebbbe capitare che il browser ci mostri
                    il nostro file come un semplice testo nonostante un file html debba impostare
                    le sezioni, i paragrafi, colonne etc... (architettura di una pagina web).
                    Nel nostro caso potrebbe non capitare dal momento che il protocollo HTTP 1.1
                    ha già definito come standard l'html per le pagine web. Avviene infatti un riconoscimento automatico
                    del testo partendo dalla prima riga "<!DOCTYPE html>".
                     */
                    out.write("Content-Type: text/html; charset=utf-8\r\n".getBytes());

                    /*  Specifichiamo ora la fine dello "request header"
                        \r serve per Unix, \n per Mac, \r\n per Windows.
                    */
                    out.write("\r\n".getBytes());

                    /*
                        Per semplificare la lettura/scrittura del codice ci serviremo ora delle lambda-expression
                        per far in modo di porre nella nostra variabile di tipo OutputStream out tutto ciò che è contenuto
                        nella variabile stream che, ricordiamo, contiene in caratteri tutto il nostro file richiesto.

                        Il metodo void forEachOrdered() serve specificare semplicemente di eseguire una funzione richiesta
                        nel suo argomento nell'ordine in cui i parametri vengono letti.

                        Le righe scritte spiegano dunque: partendo dalla variabile stream, per ogni elemento preso in modo ordinato,
                        prendi la sua stringa e scrivila, in bytes, nello flusso di uscita della socket e infine
                        poni un endline.
                     */
                    stream.forEachOrdered(line -> {
                        try{
                            out.write(line.getBytes());
                            out.write("\n".getBytes());
                        }
                        catch(IOException e){
                            new PrintStream(out).println("Errore di lettura: " + e.getMessage());
                            //stampa a video nel browser
                        }
                    });
                }
                catch(IOException e)
                {
                    if(e instanceof NoSuchFileException){
                        //Se il file non è stato trovato
                        out.write("HTTP/1.1 404 Not Found\r\n".getBytes());
                        out.write("\r\n".getBytes()); //termino l'header, in questo caso composto solo dalla stringa qui sopra
                        out.write("404 FILE NOT FOUND".getBytes()); //stampa a video nel browser
                    }
                    else{
                        out.write("HTTP/1.1 500 Internal error: \r\n ".getBytes()); //inizio e termine primo header
                        out.write(e.getMessage().getBytes()); // secondo header che può essere visualizzato dalla console del browser
                        out.write("\r\n".getBytes()); //termine secondo header
                        out.write("500 Internal error".getBytes()); //stampa a video nel browser
                    }
                }
            }

            else
            {
                /*
                Per ora, in TinyHttpd ci limiato ad accettare solo stringhe
                del tipo: GET /path/to/file
                (quindi niente POST,PUT,DELETE, etc...)
                 */
                out.write("HTTP/1.1 400 Bad request\r\n".getBytes());

                new PrintStream(out).println("Metodo ancora non supportato."); //stampa a video nel browser
            }
            //----------------------------------------------------------------------------------
        }
        catch(IOException e){
            System.err.println("Errore durante la lettura della richiesta: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        finally{
            try{
                SOCKET.close();
            }
            catch(IOException e){
                System.err.println("Errore durante la chisura della socket: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }
    }
}